<?php 
include_once('artikels/artikels_select.php');
include_once('artikels/artikels_ptj.php');
include_once('../tables/modals/modalArtikel.php');
include_once('../tables/disableModals/dModalArtikel.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Artikels</title>
<style type="text/css">
body 
{
background-color: #b0b0b0;
}
</style>
<link rel="stylesheet" type="text/css" href="../../tables/modal.css">
</script>
<script>
    //Vars of position and new items always inside the function - now are constants
    var rows = artRow
    var number = artRow[artRow.length -1];
    var nritems = 9;
    var attnames = ["artikelnummer","leveranciersnaam","artikelnaam","omschrijving","soort","afmetingen","voorraad","prijs","status"];
    var realnames = ["Artikelnummer","Leveranciersnaam","Artikelnaam","Omschrijving","Soort","Afmetingen","Voorraad","Prijs","Status"];
    var idnames = ["arc0","lvn0","arn0","oms0","tsm0","eva0","vrd0","pri0","del0"];
    var values = [arc,lvnA,arn,oms,tsm,eva,vrd,pri,delArt];
</script>
</head>
<body onload="addFields()">
<form id="artikels" method="get" action="">
<div id="pageBody">
    <div id="horizontal">
    </div>
    <div id="hideOnPrint">
    <p><u><h2>Opties</h1></u></p>
    <input type="button" id="editing" value="Bewerken modus" style="height:40px;width:180px" onclick="editAdd()">
    <input type="button" id="deleting" value="Status modus" style="height:40px;width:180px;display:none;" onclick="deleteAdd()">
    <br>
    <input type="button" value="Terug naar startscherm" style="height:40px;width:180px" onclick="location.href='../../addingitems.php','650'">
    <br>
    <input type="button" name="menu" value="Scherm afprinten" style="height:40px;width:180px" onclick="document.getElementById('hideOnPrint').style.display = 'none';window.print();location.reload()">
    </div>
    </div>
    <!-- Modal content -->
    <div id="modalPopUp" class="modal">
    <div class="modal-content">
    <input type="button" class="button close" value="&#10060;">
    <table>
    <tr>
    <td><p>Rangnummer</p>
    <td><p>Artikelnummer</p>
    <td><p>Leveranciersnaam</p>
    <td><p>Artikelnaam</p>
    <td><p>Omschrijving</p>
    <td><p>Soort</p>
    <td><p>Afmetingen</p>
    <td><p>Voorraad</p>
    <td><p>Prijs</p>
    </tr>
    <tr id="textContainerART">
    <td><input type="text" name="artRow" id="artRow" readonly >
    <td><input type="text" name="arc" id="arc" readonly >
    <td><input type="text" name="lvc" id="lvc" readonly >
    <td><input type="text" name="arn" id="arn" readonly >
    <td><input type="text" name="oms" id="oms" readonly >
    <td><input type="text" name="tsm" id="tsm" readonly >
    <td><input type="text" name="eva" id="eva" readonly >
    <td><input type="text" name="vrd" id="vrd">
    <td><input type="text" name="pri" id="pri" readonly >
    </tr>
    <input type="submit" class="button save" value="&#10004;&#65039;">
    </table>
  </div>
</div>
</form>
<script src="listmaker.js"></script>
<script src="changeTypes.js"></script>
<script src="appendEdit.js"></script>
<script src="appendDelete.js"></script>
<script src="../windowsOpen.js"></script>
</body>
</html>