-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Gegenereerd op: 29 apr 2021 om 09:00
-- Serverversie: 5.6.34
-- PHP-versie: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `materiaalkoffers`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `controles`
--

CREATE TABLE `controles` (
  `ControleNr` int(4) NOT NULL,
  `KlasNr` int(2) NOT NULL,
  `Klasnaam` int(6) NOT NULL,
  `Periode` int(1) NOT NULL,
  `KofferNr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `controles`
--

INSERT INTO `controles` (`ControleNr`, `KlasNr`, `Klasnaam`, `Periode`, `KofferNr`) VALUES
(5, 1, 2, 1, 9);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `factuur`
--

CREATE TABLE `factuur` (
  `FactuurNr` int(11) NOT NULL,
  `FactuurDatum` date NOT NULL,
  `VervalDatum` date NOT NULL,
  `Periode` int(1) NOT NULL,
  `LeerlingNr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `factuur`
--

INSERT INTO `factuur` (`FactuurNr`, `FactuurDatum`, `VervalDatum`, `Periode`, `LeerlingNr`) VALUES
(1, '2020-06-19', '2020-06-26', 1, 6),
(2, '2020-06-25', '2020-07-02', 1, 6),
(3, '2021-04-27', '2022-09-28', 1, 6);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `gebruikers`
--

CREATE TABLE `gebruikers` (
  `GebruikersNr` int(3) NOT NULL,
  `Gebruikersnaam` varchar(20) NOT NULL,
  `Wachtwoord` varchar(20) NOT NULL,
  `Gebruikerstype` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `gebruikers`
--

INSERT INTO `gebruikers` (`GebruikersNr`, `Gebruikersnaam`, `Wachtwoord`, `Gebruikerstype`) VALUES
(11, 'admin', 'admin', 'TA'),
(12, 'admin2', 'admin', 'TA');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `klas`
--

CREATE TABLE `klas` (
  `KlasNr` int(3) NOT NULL,
  `KlasNaam` varchar(35) NOT NULL,
  `Richting` varchar(6) NOT NULL,
  `Verwijderd` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `klas`
--

INSERT INTO `klas` (`KlasNr`, `KlasNaam`, `Richting`, `Verwijderd`) VALUES
(1, '2BEM', 'ELBSO', 0),
(2, '3BEM', 'ELTSO', 0),
(3, '2BME', 'BME', 0),
(4, '3BBM', 'BME', 0),
(5, '4BBM', 'BME', 0),
(8, '2ME', 'BME', 0),
(9, '', '', 1),
(10, '', '', 1),
(11, 'elbso65', 'ELBSO', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `koffer`
--

CREATE TABLE `koffer` (
  `KofferNr` int(3) NOT NULL,
  `Foto` varchar(100) NOT NULL,
  `LeerlingNr` int(2) NOT NULL,
  `Verwijderd` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `koffer`
--

INSERT INTO `koffer` (`KofferNr`, `Foto`, `LeerlingNr`, `Verwijderd`) VALUES
(1, '../images/koffer/koffer in kunststof 33cm.jpg', 1, 1),
(9, '../images/koffer/koffer in kunststof 40cm.jpg', 6, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `kofferinhoud`
--

CREATE TABLE `kofferinhoud` (
  `ID` int(4) NOT NULL,
  `KofferNr` int(3) NOT NULL,
  `MateriaalNr` int(3) NOT NULL,
  `Status` varchar(30) NOT NULL DEFAULT 'In orde',
  `Verwijderd` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `kofferinhoud`
--

INSERT INTO `kofferinhoud` (`ID`, `KofferNr`, `MateriaalNr`, `Status`, `Verwijderd`) VALUES
(18, 9, 19, 'Verloren', 0),
(19, 9, 17, 'Beschadigd', 0),
(20, 9, 20, 'Niet opzettelijk beschadigd', 0),
(21, 9, 21, 'In orde', 0),
(22, 9, 22, 'Verloren', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `leerling`
--

CREATE TABLE `leerling` (
  `LeerlingNr` int(11) NOT NULL,
  `LeerlingNaam` varchar(30) NOT NULL,
  `KlasNr` int(2) NOT NULL,
  `Verwijderd` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `leerling`
--

INSERT INTO `leerling` (`LeerlingNr`, `LeerlingNaam`, `KlasNr`, `Verwijderd`) VALUES
(1, 'Bachiri Achraf', 1, 0),
(2, 'Coppenolle Alisho', 1, 0),
(3, 'Couwet Kyano', 1, 0),
(4, 'Demeyere Remco', 8, 0),
(5, 'Samyn Stijn', 8, 0),
(6, 'Kawas Mohamad Bassam', 1, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `leveranciers`
--

CREATE TABLE `leveranciers` (
  `LeveranciersNr` int(13) NOT NULL,
  `LeveranciersNaam` varchar(35) NOT NULL,
  `Adres` varchar(40) NOT NULL,
  `Gemeente` varchar(35) NOT NULL,
  `TelefoonNr` varchar(13) NOT NULL,
  `MailAdres` varchar(40) NOT NULL,
  `Verwijderd` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `leveranciers`
--

INSERT INTO `leveranciers` (`LeveranciersNr`, `LeveranciersNaam`, `Adres`, `Gemeente`, `TelefoonNr`, `MailAdres`, `Verwijderd`) VALUES
(1, 'De Munter', 'Doorniksesteenweg 203', 'Kortrijk', '056/ 45 55 95', 'demunter@gmail.com', 0),
(2, 'Van Houten Demitri', 'Oudenaardestraat 85', 'Anzegem', '512 48 48 10', 'vanhoutendemitri@gmail.com', 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `materiaal`
--

CREATE TABLE `materiaal` (
  `MateriaalNr` int(4) NOT NULL,
  `MateriaalNaam` varchar(35) NOT NULL,
  `Omschrijving` varchar(255) NOT NULL,
  `Afbeelding` varchar(100) NOT NULL,
  `LeveranciersNr` int(13) NOT NULL COMMENT 'Verwijssleutel',
  `Aantal` int(2) NOT NULL,
  `Prijs` decimal(5,2) NOT NULL,
  `Verwijderd` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `materiaal`
--

INSERT INTO `materiaal` (`MateriaalNr`, `MateriaalNaam`, `Omschrijving`, `Afbeelding`, `LeveranciersNr`, `Aantal`, `Prijs`, `Verwijderd`) VALUES
(3, 'Vorksleutel 6-7', '35201010', '', 1, 1, '1.00', 1),
(15, 'rolmeter', 'rolmeter', '../images/koffer/rolmeter.jpg', 2, 1, '3.00', 0),
(17, 'juniorzaag', 'kleine metalen zaag', '../images/koffer/juniorzaag.jpg', 2, 1, '5.00', 0),
(18, 'Kabelmes', '', '../images/koffer/kabelmes.jpg', 1, 1, '3.00', 0),
(19, 'Multimeter', 'zwarte en rode kabels', '../images/koffer/multimeter.jpg', 2, 1, '10.50', 0),
(20, 'Priem', '', '../images/koffer/priem.jpg', 1, 1, '3.00', 0),
(21, 'Vouwmeter', 'wit met zwarte tekens', '../images/koffer/vouwmeter.jpg', 2, 1, '3.00', 0),
(22, 'Waterpas', 'een witte leitz waterpas', '../images/koffer/waterpas.jpg', 1, 1, '4.00', 0),
(23, 'houpsipap', 'a thing', '', 2, 1, '40.50', 1),
(24, 'houpsipappidoup', 'a thing', '', 2, 1, '50.50', 1);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `controles`
--
ALTER TABLE `controles`
  ADD PRIMARY KEY (`ControleNr`),
  ADD UNIQUE KEY `KlasNr` (`KlasNr`),
  ADD KEY `KofferNr` (`KofferNr`);

--
-- Indexen voor tabel `factuur`
--
ALTER TABLE `factuur`
  ADD PRIMARY KEY (`FactuurNr`),
  ADD KEY `LeerlingNr` (`LeerlingNr`);

--
-- Indexen voor tabel `gebruikers`
--
ALTER TABLE `gebruikers`
  ADD PRIMARY KEY (`GebruikersNr`);

--
-- Indexen voor tabel `klas`
--
ALTER TABLE `klas`
  ADD PRIMARY KEY (`KlasNr`),
  ADD UNIQUE KEY `KlasNr` (`KlasNr`);

--
-- Indexen voor tabel `koffer`
--
ALTER TABLE `koffer`
  ADD PRIMARY KEY (`KofferNr`),
  ADD KEY `LeerlingNr` (`LeerlingNr`);

--
-- Indexen voor tabel `kofferinhoud`
--
ALTER TABLE `kofferinhoud`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `KofferNr` (`KofferNr`);

--
-- Indexen voor tabel `leerling`
--
ALTER TABLE `leerling`
  ADD PRIMARY KEY (`LeerlingNr`),
  ADD KEY `KlasNr` (`KlasNr`);

--
-- Indexen voor tabel `leveranciers`
--
ALTER TABLE `leveranciers`
  ADD PRIMARY KEY (`LeveranciersNr`);

--
-- Indexen voor tabel `materiaal`
--
ALTER TABLE `materiaal`
  ADD PRIMARY KEY (`MateriaalNr`),
  ADD KEY `LeveranciersNr` (`LeveranciersNr`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `controles`
--
ALTER TABLE `controles`
  MODIFY `ControleNr` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT voor een tabel `factuur`
--
ALTER TABLE `factuur`
  MODIFY `FactuurNr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT voor een tabel `gebruikers`
--
ALTER TABLE `gebruikers`
  MODIFY `GebruikersNr` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT voor een tabel `klas`
--
ALTER TABLE `klas`
  MODIFY `KlasNr` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT voor een tabel `koffer`
--
ALTER TABLE `koffer`
  MODIFY `KofferNr` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT voor een tabel `kofferinhoud`
--
ALTER TABLE `kofferinhoud`
  MODIFY `ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT voor een tabel `leerling`
--
ALTER TABLE `leerling`
  MODIFY `LeerlingNr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT voor een tabel `leveranciers`
--
ALTER TABLE `leveranciers`
  MODIFY `LeveranciersNr` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT voor een tabel `materiaal`
--
ALTER TABLE `materiaal`
  MODIFY `MateriaalNr` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `controles`
--
ALTER TABLE `controles`
  ADD CONSTRAINT `KofferNr` FOREIGN KEY (`KofferNr`) REFERENCES `koffer` (`KofferNr`),
  ADD CONSTRAINT `controlesklasconstraint` FOREIGN KEY (`KlasNr`) REFERENCES `klas` (`KlasNr`);

--
-- Beperkingen voor tabel `factuur`
--
ALTER TABLE `factuur`
  ADD CONSTRAINT `constr_factuurleerling` FOREIGN KEY (`LeerlingNr`) REFERENCES `leerling` (`LeerlingNr`);

--
-- Beperkingen voor tabel `koffer`
--
ALTER TABLE `koffer`
  ADD CONSTRAINT `LeerlingNr` FOREIGN KEY (`LeerlingNr`) REFERENCES `leerling` (`LeerlingNr`);

--
-- Beperkingen voor tabel `kofferinhoud`
--
ALTER TABLE `kofferinhoud`
  ADD CONSTRAINT `kofferinhoud_ibfk_1` FOREIGN KEY (`KofferNr`) REFERENCES `koffer` (`KofferNr`);

--
-- Beperkingen voor tabel `leerling`
--
ALTER TABLE `leerling`
  ADD CONSTRAINT `leerklasconstraint` FOREIGN KEY (`KlasNr`) REFERENCES `klas` (`KlasNr`);

--
-- Beperkingen voor tabel `materiaal`
--
ALTER TABLE `materiaal`
  ADD CONSTRAINT `LeveranciersNr_1` FOREIGN KEY (`LeveranciersNr`) REFERENCES `leveranciers` (`LeveranciersNr`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
