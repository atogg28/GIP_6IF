<!DOCTYPE html>

<html>
<head>
<title>Formulier Klas</title>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="formulieren.css">
</head>
<body>
<h1>Invoeren van klassen</h1>
<hr>


<?php
session_start();
$servername = "localhost";
$DB = "materiaalkoffers";
$User= "root";
$Password = "usbw";

// verbinding met de databank maken
$conn = new mysqli($servername, $User, $Password,$DB);

// verbinding checken
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
if ($conn === false)
{
     die("ERROR" . mysqli_connect_error());}

     

else 
{echo mysqli_error($conn);}


?>

<form name="Table" method="post" action="Form_InvoerKlas.php" style="margin:20px 0px;">
<table border="2" style="width:500px;">
<tr>
<td>Richting: </td>

<td>
<?php
// dropdown list for classes, select where richting = $ richting
  $resultset = $conn->query("SELECT DISTINCT Richting FROM klas");

?>
<input type="text" list="Richting" name="Richting" required/>
<datalist id="Richting">
  <?php
  while($rows = $resultset->fetch_assoc())
  {
    $richt_row = $rows['Richting'];
    echo "<option value='$richt_row'>$richt_row</option>";
  }
  ?>
</datalist>
</td>

</tr>
<tr>
<td>Klas naam: </td>
<td><input type="text" name="KlasNaam" required></td>
</tr>
</table>
<input type="submit" value = "Ingeven">
<input type="button" value="Raadplegen" onclick="location.href='Form_RaadKlas.php'">
<input type="button" value="Terug" onclick="location.href='../../Welkom.html'">
</form>

</body>
</html>