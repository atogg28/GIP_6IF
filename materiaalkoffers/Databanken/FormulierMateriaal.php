<!DOCTYPE html>

<html>
<head>
<title>Formulier Materiaal</title>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="formulieren.css">
</head>
<body>
<h1>Invoeren van materiaal</h1>
<hr>

<?php
session_start();
$servername = "localhost";
$DB = "materiaalkoffers";
$User= "root";
$Password = "usbw";

// verbinding met de databank maken
$conn = new mysqli($servername, $User, $Password,$DB);

// verbinding checken
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
if ($conn === false)
{
     die("ERROR" . mysqli_connect_error());}

     

else 
{echo mysqli_error($conn);}


?>

<form name="Table" method="post" action="Form_InvoerMateriaal.php">
<table border="2">
<tr>
<td>Materiaal Naam: </td>
<td><input type="text" name="MateriaalNaam" required></td>
</tr>
<tr>
<td>Omschrijving:  </td>
<td><input type="text" name="Omschrijving"></td>
</tr>
<tr>
<td>Afbeelding (+ extention):  </td>
<td><input type="text" name="Afbeelding"></td>
</tr>

<tr>
<td>Leverancier: </td>

<td>
<?php

  $resultset = $conn->query("SELECT LeveranciersNr, LeveranciersNaam FROM leveranciers");

?>
<select name="Leverancier" required>
  <?php
  while($rows = $resultset->fetch_assoc())
  {
    $richt_row = $rows['LeveranciersNaam'];
    $levnr_row = $rows['LeveranciersNr'];
    echo "<option value='$levnr_row'>$richt_row</option>";
  }
  ?>
</select>
</td>

</tr>

<tr>
<td>Aantal:  </td>
<td><input type="number" name="Aantal" required></td>
</tr>
<tr>
<td>Prijs:  </td>
<td><input type="number" min="0.01" step="0.01" max="2500" name="Prijs" required></td>
</tr>
</table>
<input type="submit" value = "Ingeven">
<input type="button" value="Raadplegen" onclick="location.href='Form_RaadMateriaal.php'">
<input type="button" value="Terug" onclick="location.href='../../Welkom.html'">
</form>
</body>
</html>