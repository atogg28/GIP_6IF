<!DOCTYPE html>

<html>
<head>
<title>Formulier Koffer</title>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="formulieren.css">

</head>
<body>
<center><h1>Invoeren van koffers</h1></center>
<hr>

<?php
session_start();
$servername = "localhost";
$DB = "materiaalkoffers";
$User= "root";
$Password = "usbw";

// verbinding met de databank maken
$conn = new mysqli($servername, $User, $Password,$DB);

// verbinding checken
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
if ($conn === false)
{
     die("ERROR" . mysqli_connect_error());}

     

else 
{echo mysqli_error($conn);}


?>

<br />

<form name="Table" method="post" action="Form_InvoerKoffer.php">
<table border="2">
<center><h2 style="padding-bottom:10px">Koffer toevoegen</h2></center>

<tr>
<td>Leerling naam: </td>

<td>
<?php

  $resultset = $conn->query("SELECT leerling.LeerlingNr, leerling.LeerlingNaam FROM leerling LEFT JOIN koffer ON koffer.LeerlingNr = leerling.LeerlingNr WHERE koffer.LeerlingNr IS NULL OR koffer.Verwijderd = 1");

?>
<select name="LeerlingNr" required>
  <?php
  while($rows = $resultset->fetch_assoc())
  {
    $leerlingnaam_row = $rows['LeerlingNaam'];
    $leerlingnr_row = $rows['LeerlingNr'];
    echo "<option value='$leerlingnr_row'>$leerlingnaam_row</option>";
  }
  ?>
</select>
</td>

</tr>

<tr>
<td>Afbeelding (+ extention):  </td>
<td><input type="text" name="Foto"></td>
</tr>

</table>
<input type="submit" value = "Ingeven">
</form>



<form name="Materiaal" method="post" action="Form_InvoerKofferInhoud.php">

<table border="2">
<center><h2 style="padding-bottom:10px">Materiaal toevoegen aan koffer</h2></center>

<tr>
<td>Leerling:</td>

<td>
<?php

  $resultset = $conn->query("SELECT koffer.LeerlingNr, leerling.LeerlingNaam FROM leerling INNER JOIN koffer ON koffer.LeerlingNr = leerling.LeerlingNr WHERE koffer.Verwijderd = 0");

?>
<select name="LeerlingNr" required>
  <?php
  while($rows = $resultset->fetch_assoc())
  {
    $llgnr_row = $rows['LeerlingNr'];
    $llgnaam_row = $rows['LeerlingNaam'];
    echo "<option value='$llgnr_row'>$llgnaam_row</option>";
  }
  ?>
</select>
</td>
</tr>

<tr>
<td>Materiaal:</td>

<td>
<?php

  $resultset = $conn->query("SELECT MateriaalNr, MateriaalNaam FROM materiaal WHERE Verwijderd = 0");

?>
<select name="MateriaalNr" required>
  <?php
  while($rows = $resultset->fetch_assoc())
  {
    $matnr_row = $rows['MateriaalNr'];
    $matnaam_row = $rows['MateriaalNaam'];
    echo "<option value='$matnr_row'>$matnaam_row</option>";
  }
  ?>
</select>
</td>

</tr>

</table>

<input type="submit" value = "Toevoegen">
<input type="button" value="Raadplegen" onclick="location.href='Form_RaadKoffer.php'">
<input type="button" value="Terug"onclick="location.href='../../Welkom.html'">
</form>




</body>
</html>
