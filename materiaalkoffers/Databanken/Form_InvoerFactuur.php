<?php 
session_unset();
include_once('config.php');

// Now() 
$factuurdtm = date('Y/m/d');
$vervaldtm = $_POST['VervalDatum'];
$periode = $_POST['Periode'];
$leerlingnr = $_POST['Leerling'];

// In de databank moet dat een bepaalde format hebben, maar ik wil ook een variable maken met een belgische format
$factdtmfixed = date('d-m-Y', strtotime($factuurdtm)); //date format
$vervaldtmfixed = date('d-m-Y', strtotime($vervaldtm)); //date format

$totaal = 0;
$totaalmetBTW = 0;

// FACTUUR RECORD MAKEN

// Lokaal zetten voor valuta, later gebruikt
setlocale(LC_MONETARY, 'it_IT');

$sql = "INSERT INTO materiaalkoffers.factuur (FactuurDatum, Periode, LeerlingNr, VervalDatum) VALUES ('$factuurdtm','$periode','$leerlingnr', '$vervaldtm')";

 if (mysqli_query($conn, $sql))
 {}
 else 
 {echo mysqli_error($conn);}

?>



<p>Factuur datum:
<?php
// FACTUUR TONEN
echo $factdtmfixed;
$_SESSION['factuurdatum'] = $factdtmfixed;

?>
</p>

<p>Vervaldatum:
<?php
echo $vervaldtmfixed;
$_SESSION['vervaldatum'] = $vervaldtmfixed;
?>
</p>


<?php
// KOFFER INFORMATIE VAN LEERLING ZOALS KOFFERNR....
 $sql = "SELECT koffer.*, leerling.LeerlingNaam, klas.KlasNaam FROM leerling, koffer, klas WHERE leerling.LeerlingNr = Koffer.LeerlingNr AND klas.KlasNr = leerling.KlasNr AND leerling.LeerlingNr = " . $leerlingnr;
$result =mysqli_query($conn,$sql);



if (mysqli_query($conn, $sql))
{

    echo '<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="formulieren.css">'

        . '<table align="center" border="1px" style="width: 600px; line-height:40px;">';
    while($rows=mysqli_fetch_assoc($result))
    {
        echo '<thead>'
        . '<tr>'
        . "<td>KofferNr</td>" 
        . "<td>Leerling naam</td>"
        . "<td>Klas</td>"
        . "<td>Foto</td>"
        . "</ tr>"
        . '</thead>';

        echo "<tr>";
        echo '<tbody>';
        $_SESSION['KofferNr'] = $rows['KofferNr'];
        $_SESSION['LeerlingNaam'] = $rows['LeerlingNaam'];
        $_SESSION['KlasNaam'] = $rows['KlasNaam'];
        echo "<td>" . $rows['KofferNr'] . "</td>"
           . "<td>" . $rows['LeerlingNaam']. "</td>"
           . "<td>" . $rows['KlasNaam']. "</td>"
           . "<td><img src=" . '"' . $rows['Foto'] . '"' . 'width="200" height="100">'
       
        . '</tr>';
        echo '</tbody>';
        // ALLE MATERIALEN VAN KOFFER TONEN EN TOTAAL VAN PRIJS BEREKENEN
        $sql2 = "SELECT materiaal.MateriaalNaam, materiaal.Prijs, materiaal.Omschrijving, kofferinhoud.Status FROM materiaal, kofferinhoud WHERE materiaal.MateriaalNr = kofferinhoud.MateriaalNr AND kofferinhoud.KofferNr = " . $rows['KofferNr'] . " AND kofferinhoud.Status <> 'In orde' AND kofferinhoud.Status <> 'Niet opzettelijk beschadigd'";
        $result2 = mysqli_query($conn, $sql2);
        echo '<thead><tr><td>Materiaal naam</td><td>Status</td><td>Prijs</td></tr>';
        if (mysqli_query($conn,$sql2))
        {
            $materiaalnamen = array();
            $prices = array();
            $omschrijvingen = array();
            $statusen = array();
            while($rows2=mysqli_fetch_assoc($result2))
            {
                
                array_push($materiaalnamen, $rows2['MateriaalNaam']);
                
                array_push($statusen, $rows2['Status']);
                
                array_push($prices, $rows2['Prijs']);
                
                array_push($omschrijvingen,$rows2['Omschrijving']);

                echo '<tr>';
                echo '<tbody>';
                echo "<td>" . $rows2['MateriaalNaam'] . "</td>";
                echo "<td>" . $rows2['Status'] . "</td>";
                echo "<td>" . number_format($rows2['Prijs'], 2, ',', '.') . "</td>";
                $totaal += $rows2['Prijs'];
                echo '</tbody>';
                echo '</tr>';
            }
            
        }
        
    }
}
    echo '<thead><tr>'
.    '<td> </td><td> </td><td> </td></tr></thead>';
    // TOTAAL BEDRAG TONEN
    $totaalmetBTW = $totaal + ($totaal * 0.21);

    $_SESSION['materiaalnamen'] = $materiaalnamen;
    $_SESSION['statusen'] = $statusen;
    $_SESSION['prices'] = $prices;
    $_SESSION['totaal'] = $totaal;
    $_SESSION['totaalmetBTW'] = $totaalmetBTW;
    $_SESSION['omschrijvingen'] = $omschrijvingen;

    echo '<tr>'
    . '<td> </td>'
    . '<td>Totaal:</td>'
    . "<td>" . number_format($totaal, 2, ',', '.') ."</td>";
    // TOTAAL BEDRAG INCLUSIEF BTW TONEN
    echo '<tr>'
    . '<td> </td>'
    . '<td>Totaal inclusief BTW:</td>'
    . "<td>" . number_format($totaalmetBTW, 2, ',', '.') ."</td>";
    echo '</table>';
?>

<input type="button" style="width:150px;" value="PDF Exporteren" onclick="location.href='fpdfpagina.php'">
<input type="button" value="Terug" onclick="location.href='../../Welkom.html'">