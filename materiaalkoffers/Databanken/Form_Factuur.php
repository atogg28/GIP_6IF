<!DOCTYPE html>

<html>
<head>
<title>Formulier Factuur</title>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="formulieren.css">


<?php
include_once('config.php');
?>

</head>
<body>

<img src="../images/athena.png" style="float:right;" width="150" height="80"></img>
<p>Athena Campus Heule</p>
<p>Guido Gezellelaan, 8501 Kortrijk</p>
<p>056 35 13 82</p>

<br />
<hr>
<br />
<h1 align="center" style="text-decoration:underline;" >Factuur</h1>


<form name="Table" method="post" action="Form_InvoerFactuur.php">
<table border="2">

<tr>
<td>Vervaldatum: </td>
<td><input type="date" name="VervalDatum" required></td>
</tr>

<tr>
<td>Periode: </td>
<td> <select name="Periode" style="float:left;" required>
         <option value="1">Periode 1</option>
				 <option value="2">Periode 2</option>
				 <option value="3">Periode 3</option>
				 <option value="4">Periode 4</option>
         </select></td>
</tr>
</table>

<br />

<table border="2">

<tr>
<td>Leerling naam: </td>

<td>
<?php

  $resultset = $conn->query("SELECT DISTINCT leerling.LeerlingNr, leerling.LeerlingNaam FROM leerling INNER JOIN koffer ON koffer.LeerlingNr = leerling.LeerlingNr INNER JOIN kofferinhoud ON kofferinhoud.koffernr = koffer.koffernr WHERE kofferinhoud.Status <> 'In orde'");

?>
<select name="Leerling" required>
  <?php
  while($rows = $resultset->fetch_assoc())
  {
    $leerlingnaam_row = $rows['LeerlingNaam'];
    $leerlingnr_row = $rows['LeerlingNr'];
    echo "<option value='$leerlingnr_row'>$leerlingnaam_row</option>";
  }
  ?>
</select>
</td>

</tr>

</table>  


<input type="submit" value="Ingeven">
<input type="button" value="Terugkeren" onclick="location.href='../../Welkom.html'">

</form>

</body>
</html>