<!DOCTYPE html>

<html>
<!--formulier voor het invoeren van nieuwe leveranciers-->
<head>
<title>Formulier Leveranciers</title>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="formulieren.css">
</head>
<body>
<h1 align="center">Invoeren van Leveranciers</h1>
<hr>
<form name="Table" method="post" action="Form_InvoerLeveranciers.php">
<table>
<td>Leveranciers naam: </td>
<td><input type="text" name="LeveranciersNaam" required></td>
</tr>
<tr>
<td>Adres:  </td>
<td><input type="text" name="Adres" required></td>
</tr>
<tr>
<td>Gemeente:  </td>
<td><input type="text" name="Gemeente" required></td>
</tr>
<tr>
<td>Telefoonnummer:  </td>
<td><input type="tel" name="TelefoonNr"></td>
</tr>
<tr>
<td>E-mail:  </td>
<td><input type="email" name="email" required></td>
</tr>
</table>
<input type="submit" value = "Ingeven">
<input type="button" value="Raadplegen" onclick="location.href='Form_RaadLeveranciers.php'">
<input type="button" value="Terug" onclick="location.href='../../Welkom.html'">
</form>
</body>
</html>
