<?php

session_start();

// variables

// koffer variables, all fixed
$koffernr = $_SESSION['KofferNr'];
$leerlingnaam = $_SESSION['LeerlingNaam'];
$klas = $_SESSION['KlasNaam'];
$vervaldatum = $_SESSION['vervaldatum'];
$factuurdatum = $_SESSION['factuurdatum'];


// materialen fixed variables
$totaal = $_SESSION['totaal'];
$totaalmetBTW = $_SESSION['totaalmetBTW'];

$totaal = number_format($totaal, 2, ',', '.');
$totaalmetBTW = number_format($totaalmetBTW, 2, ',', '.');


$_SESSION['orderlijnen'] = array(
    $_SESSION['materiaalnamen'], $_SESSION['omschrijvingen'], $_SESSION['statusen'], $_SESSION['prices']
);


require('fpdf.php');

class PDF extends FPDF{
    function Header() { 
    //Logo
    $this->Image('Afbeeldingen/Welkom/AThena logo.png',10,8,50,30); //Arial bold 15 
    $this->SetFont('Arial','B',15); //Move to the right 
    $this->SetXY(120, 10); //Title 
    $this->Cell(75,10,'Athena Heule' ,0,0,'L'); 
    $this->SetFont('Arial','',10); 
    $this->SetXY(120, 15); 
    $this->Cell(75,10,'Guido Gezellelaan 10 8501 Heule' ,0,0,'L'); 
    $this->SetXY(120, 20); 
    $this->Cell(75,10,'1ste graad + OV4 Type 9: Tel. 056 37 03 57' ,0,0,'L'); //Line break 
    $this->SetXY(120, 25); 
    $this->Cell(75,10,'2de en 3de graad: Tel. 056 35 13 82 ' ,0,0,'L'); //Line break 
    $this->SetXY(120, 30); 
    $this->Cell(75,10,'info@athena-heule.be' ,0,0,'L'); //Line break 
    $this->Ln(20); // de ordernummer + waarschuwingsbox 
    $this->SetXY(10,45); 
    $this->SetFillColor(200,200,200); 
    $this->Cell(0,15,"",1,1,'C',1); 
    $this->SetXY(10,43); 
    $this->SetFillColor(200,200,200); 
    $this->SetFont('Arial','B',25); 
    $this->Cell(0,20,'Factuur ' ,0,0,'C'); 
    $this->Ln(20); }


//Page footer 
function Footer() { //Position at 1.5 cm from bottom
    $this->SetY(-15);
    //Arial italic 8 
    $this->SetFont('Arial','I',8); 
     //Page number
      $this->Cell(0,10,'Pagina '
    . $this->PageNo().'/{nb} - Gemaakt door Devon Brazelton en Keanu Coussement 2020 ',0,0,'C'); }
}

//Instanciation of inherited class
 $pdf=new PDF(); 
 $pdf->AliasNbPages();
  $pdf->AddPage(); 
  $pdf->SetFont('Times','',12); 
  $pdf->Line(10,40,200,40);


// factuur datum, vervaldatum
// leerling naam
$pdf->SetFillColor(255,255,255); 
$pdf->SetFont('Arial','B',10); 
$pdf->SetXY(10,60);
$pdf->Cell(50,5,"Factuurdatum: " . $factuurdatum,1,0,'C',1); 
$pdf->Cell(50,5,"Vervaldatum: " . $vervaldatum,1,0,'C',1); 
$pdf->Cell(90,5,"Leerling: " . $leerlingnaam,1,0,'C',1); 


  // de lijnen van de order
   // artikel naam - omschrijving - staat - prijs
    // de HEADER
$pdf->SetFillColor(200,200,200); 
$pdf->SetFont('Arial','B',12); 
$pdf->SetXY(10,80); 
$pdf->Cell(50,5,"Artikel Naam",1,0,'C',1); 
$pdf->Cell(85,5,"Omschrijving",1,0,'C',1); 
$pdf->Cell(30,5,"Staat",1,0,'C',1); 
$pdf->Cell(25,5,"Prijs",1,0,'C',1); 
$pdf->Ln(5);

// de artikels...
$pdf->SetFont('Arial','',10); 

/*HIER MOET ER GELOOPT WORDEN OP DE ORDERLIJNEN BINNEN DE ORDER*/

for ($i=0;$i<count($_SESSION['materiaalnamen']);$i++){
        
        // COLOR OF TABLE, EVERY OTHER ROW IS DIFF COLOR
        if (($i%2) != 0)
        { $pdf->SetFillColor(220,220,220); } 
        else{ $pdf->SetFillColor(255,255,255); }
        
            $key = $_SESSION['materiaalnamen'][$i];
            $pdf->Cell(50,15,$key,'LR' . 'B',0,'L',true);

            $key = $_SESSION['omschrijvingen'][$i];
            $pdf->Cell(85,15,$key,'LR' . 'B',0,'L',true);

            $key = $_SESSION['statusen'][$i];
            $pdf->Cell(30,15,$key,'LR' . 'B',0,'L',true);

            $key = $_SESSION['prices'][$i];
            $pdf->Cell(25,15, $key,'LR' . 'B',0,'L',true);

            
            $pdf->Ln(15);
        }
        // totaal EXCLUSIEF BTW
        $pdf->SetX(145);
        $pdf->SetFillColor(220,220,220);
        $pdf->Cell(30,15,"Totaal",1,0,'C',1); 

        $pdf->SetX(175);
        $pdf->SetFillColor(255,255,255);
        $pdf->Cell(25,15,$totaal,1,0,'L',true);

        //BTW
        $btw = $totaalmetBTW - $totaal;
        $btw = number_format($btw, 2, ',', '.');
        $pdf->ln(15);
        $pdf->SetX(145);
        $pdf->SetFillColor(220,220,220);
        $pdf->Cell(30,15,"BTW",1,0,'C',1); 

        $pdf->SetFillColor(255,255,255);
        $pdf->SetX(175);
        $pdf->Cell(25,15,$btw,1,0,'L',true);

        // INCLUSIEF BTW
        $pdf->ln(15);
        $pdf->SetX(145);
        $pdf->SetFillColor(220,220,220);
        $pdf->Cell(30,15,"Incl. BTW",1,0,'C',1);

        $pdf->SetFillColor(255,255,255);
        $pdf->SetX(175);
        $pdf->Cell(25,15,$totaalmetBTW,1,0,'L',true);

        // NAAM OUDERS LABEL
        $pdf->ln(40);
        $pdf->SetX(10);
        $pdf->SetFillColor(255,255,255);
        $pdf->Cell(47,15,"Naam ouder(s)/verzorger(s):",0,0,'L',true);

        // SCHOOL HANDTEKENING LABEL
        $pdf->SetX(110);
        $pdf->Cell(47,15,"Handtekening school:",0,0,'L',true);

        // NAAM OUDERS RECHTHOEK
        $pdf->ln(15);
        $pdf->Cell(80,15,"",1,0,'L',true);

        // SCHOOL HANDTEKENING RECHTHOEK
        $pdf->SetX(110);
        $pdf->Cell(80,15,"",1,0,'L',true);

        // HANDTEKENING OUDERS LABEL
        $pdf->ln(16);
        $pdf->Cell(30,15,"Handtekening:",0,0,'L',true);

        // SCHOOL DATUM EN PLAATS LABEL
        $pdf->SetX(110);
        $pdf->Cell(30,15,"Datum/Plaats:",0,0,'L',true);

        //  HANDTEKENING OUDERS RECHTHOEK
        $pdf->ln(15);
        $pdf->Cell(80,15,"",1,0,'L',true);

        // SCHOOL DATUM EN PLAATS RECHTHOEK
        $pdf->SetX(110);
        $pdf->Cell(80,15,"",1,0,'L',true);

        


    
$pdf->output();

?>