<!DOCTYPE html>


<html>
<head>
<title>Formulier Leerling</title>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="formulieren.css">

<?php
session_start();
$servername = "localhost";
$DB = "materiaalkoffers";
$User= "root";
$Password = "usbw";

// verbinding met de databank maken
$conn = new mysqli($servername, $User, $Password,$DB);

// verbinding checken
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
if ($conn === false)
{
     die("ERROR" . mysqli_connect_error());}

     

else 
{echo mysqli_error($conn);}


?>
</head>

<body>
<h1>Invoeren van leerlingen</h1>
<hr>
<form name="Table" method="post" action="Form_InvoerLeerling.php">
<table border="2">
<tr>
<td>LeerlingNaam: </td>
<td><input type="text" name="LeerlingNaam"></td>
</tr>

<tr>
<td>Klas: </td>

<td>
<?php

  $resultset = $conn->query("SELECT KlasNr, KlasNaam FROM klas");

?>
<select name="KlasNr" required>
  <?php
  while($rows = $resultset->fetch_assoc())
  {
    $klasnaam_row = $rows['KlasNaam'];
    $klasnr_row = $rows['KlasNr'];
    echo "<option value='$klasnr_row'>$klasnaam_row</option>";
  }
  ?>
</select>
</td>

</tr>

</table>
<input type="submit" value = "Ingeven">
<input type="button" value="Raadplegen" onclick="location.href='Form_RaadLeerling.php'">
<input type="button" value="Terug"onclick="location.href='../../Welkom.html'">
</form>
</body>
</html>