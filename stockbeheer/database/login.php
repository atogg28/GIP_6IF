<!-- php-->
<?php
include_once('config/permschecker.php')
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="loginmat.css" rel="stylesheet" type="text/css" media="screen" />
<title>Stockbeheer - Inloggen</title>
<link rel="stylesheet" type="text/css" href="responsive.css">

</style>
</head>
<body><!-- lay-out-->
    <form method="post">
        <div style ="margin-left:4%">
        <div style = "width:300px; border: solid 1px #333333;">
        <div style = "background-color:#333333; color:#ffffff; padding:3px;"><strong>Stockbeheer - Login</strong></div>
        <div style = "margin:30px">
<table>
<tr>
    <td>User:</td><!-- veld voor gebruikersnaam-->
    <td><input type="text" name="user" id="user"><br></td>
</tr>
<tr>
    <td>Pass:</td><!-- verld voor wachtwoord-->
    <td><input type="password" name="pass" id="pass"><br></td>
</tr>
<tr>
    <!-- je wordt ingelogd indien de gegevens juist zijn-->
    <td><input type="submit" name="login" id="login" value="Login"></td>
    <td><input type="button" value="Terug" onclick="location.href='../../index.php'"></td>
</tr>
</table>
        </div>
        </div>
        </div>
    </form>
</body>
</html>
