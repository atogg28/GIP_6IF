function openlogin() {
    window.open('login.php', '_blank', 'left=200px, top=200px, toolbar=no, menubar=no, resizable=no, scrollbars=no, status=no, height=200px, width=350px')
}

function openStart(path, height) {
    var targetafterlogin = window.open(path, '_self', 'height=650px, width=300px')
    targetafterlogin.moveTo(0, 0)
    targetafterlogin.resizeTo(300, height)
}

function openStartForForms(path) {
    var targetafterform = window.open(path, '_blank', 'height=550px, width=800px')
    targetafterform.moveTo(0, 0)
    window.close('_self')

}

function openStartForPages(path, height) {
    var targetafterpage = window.open(path, '_blank', 'height='+height+', width=300px')
    targetafterpage.moveTo(0, 0)
    targetafterpage.resizeTo(300, height)
    window.close('_self')

}

function openStartForTables(path) {
    var targetaftertable = window.open(path, '_blank', 'height=550px, width=1600px')
    targetaftertable.moveTo(0, 0)
    window.close('_self')

}