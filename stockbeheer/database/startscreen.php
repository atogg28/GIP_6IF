<?php 
require_once('config/config.php');
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Startscherm</title>

    <link rel="stylesheet" type="text/css" href="responsive.css">

</style>
</head>
<body>
<table>
<tr>
<td><p>Ingelogde gebruiker: <?php print $_SESSION['user']; ?></p></td>
</tr>
<tr>       
</tr>
<!-- deze button is een link naar de handleiding van de website stockbeheer -->
<div class="page">
<input type="button" value="Handleiding" style="height:40px;width:120px" onclick="window.open('Handleiding.pdf','_blank')"></td>
<tr>     <!--dit is een link naar de leveranciers -->
    <td><br><p class="title">Leveranciers</p>
    <tr><td><a href="tables/leveranciers.php" onclick="location.href('tables/leveranciers.php')">Overzicht</a></td></tr>
    <tr><td><a href="forms/for_TA/leveranciers_form.php" onclick="location.href('forms/for_TA/leveranciers_form.php')">Formulier</a></td></tr>
    </td>


        <!--dit is een link naar de artikels-->
    <td><br><p class="title">Artikels</p>
    <tr><td><a href="tables/artikels.php" onclick="location.href('tables/artikels.php')">Overzicht</a></td></tr>

    <tr><td><a href="forms/for_TA/artikels_form.php" onclick="location.href('forms/for_TA/artikels_form.php')">Formulier</a></td></tr>
    </td>


        <!-- dit is een link naar de bestelling-->
    <td><br><p class="title">Bestelling</p>
    <tr><td><a href="tables/bestelling.php" onclick="location.href('tables/bestelling.php')">Overzicht</a></td></tr>

    <tr><td><a href="forms/for_TA/bestelling_form.php" onclick="location.href('forms/for_TA/bestelling_form.php')">Formulier</a></td></tr>
    </td>
</tr>
<tr> <!-- deze button logt je uit uit de website -->
    <td><br><br><input type="button" value="Uitloggen" style="height:40px;width:120px" onclick="location.href='../../index.php','650'">
    
   
</tr>
</table>
</div>
<div class="image">
<img src="../../materiaalkoffers/images/stockbeheerstartpagina.jpg" alt="beautiful" width="100%" height= "100%">

</div>
</body>
<!-- Deze pagine is hoofdzakelijk lay-out en links naar andere delen van de website. -->
<script src="windowsOpen.js"></script>

</html>