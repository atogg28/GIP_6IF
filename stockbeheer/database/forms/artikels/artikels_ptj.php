<?php
$artRowArrayString = $_SESSION['artRowArray'];
$artRowArray = array_map('intval',$artRowArrayString);
$arcArray = $_SESSION['arcArray'];
$lvcAArray = $_SESSION['lvcAArray'];
$lvnAArray = $_SESSION['lvnAArray'];
$arnArray = $_SESSION['arnArray'];
$omsArray = $_SESSION['omsArray'];
$tsmArray = $_SESSION['tsmArray'];
$evaArray = $_SESSION['evaArray'];
$vrdArray = $_SESSION['vrdArray'];
$priArray = $_SESSION['priArray'];
$delArt = $_SESSION['delArt'];
echo '
<script>
var artRow = '.json_encode($artRowArray).'  
var arc = '.json_encode($arcArray).'
var lvc = '.json_encode($lvcAArray).'
var lvnA = '.json_encode($lvnAArray).'
var arn = '.json_encode($arnArray).'
var oms = '.json_encode($omsArray).'
var tsm = '.json_encode($tsmArray).'
var eva = '.json_encode($evaArray).'
var vrd = '.json_encode($vrdArray).'
var pri = '.json_encode($priArray).'
var delArtNumeric = '.json_encode($delArt).'
var delArtOne = []
var delArtZero = []
delArtOne = delArtNumeric.map(function( one){
    return one == 1 ? "uitgeschakeld": one});
delArtZero = delArtOne.map(function(zero){
    return zero == 0 ? "ingeschakeld": zero});
var delArt = delArtZero
</script>';
?>