<?php
$levRowArrayString = $_SESSION['levRowArray'];
$levRowArray = array_map('intval',$levRowArrayString);
$lvcArray = $_SESSION['lvcArray'];
$lvnArray = $_SESSION['lvnArray'];
$strArray = $_SESSION['strArray'];
$gmtArray = $_SESSION['gmtArray'];
$emlArray = $_SESSION['emlArray'];
$delLevArray = $_SESSION['delLev'];
echo '
<script>
var levRow = '.json_encode($levRowArray).'
var lvc = '.json_encode($lvcArray).'
var lvn = '.json_encode($lvnArray).'
var str = '.json_encode($strArray).'
var gmt = '.json_encode($gmtArray).'
var eml = '.json_encode($emlArray).'
var delLevNumeric = '.json_encode($delLevArray).'
var delLevOne = []
var delLevZero = []
delLevOne = delLevNumeric.map(function( one){
    return one == 1 ? "uitgeschakeld": one});
delLevZero = delLevOne.map(function(zero){
    return zero == 0 ? "ingeschakeld": zero});
var delLev = delLevZero
</script>'
?>
