<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Formulier artikels</title>

<style type="text/css">
body {
    font-family: Segoe UI Semilight;
    color:blue;
    background-color: #b0b0b0;

}
p {border-style:double;
    background-color:cyan;
}
</style>
</head>
<body>
<form name="artikels" method="get" action="../artikels/artikels_query.php">
<table>
    <tr>
        <td><p>Artikelnummer</p></td><td><input type="text" name="arc" id="arc" size="10" required></td>
    </tr>
    <tr>
        <td><p>Leveranciersnaam</p></td><td>
            <select name="lvc" id="lvc" required>
                <!--Selection of leveranciers-->
                <?php 
                    include_once('../../config/config.php');
                    
                    $levRow = [];
                    $lvcArray = [];
                    $lvnArray = [];
                    $sql = "SELECT levRow, lvc, lvn FROM leveranciers";
                    $result = mysqli_query($connection,$sql);
                            if (mysqli_query($connection,$sql)){   
                                while($rows=mysqli_fetch_assoc($result)){
                                    array_push($levRow, $rows['levRow']);
                                    array_push($lvcArray, $rows['lvc']);
                                    array_push($lvnArray, $rows['lvn']);
                                    
                                }
                                for($i=0;$i < max($levRow);$i++)
                                {
                echo '<option value='.$lvcArray[$i].'>'.$lvnArray[$i].'</option>';
                                }
                                    }
            
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td><p>Artikelnaam</p></td><td><input type="text" name="arn" id="arn" size="40" required></td>
    </tr>
    <tr>
        <td><p>Omschrijving</p></td><td><textarea name="oms" id="oms" rows="4" cols="30" required></textarea></td>
    </tr>
    <tr>
        <td><p>Soort</p></td><td><input type="text" name="tsm" id="tsm" size="50" required></td>
    </tr>
    <tr>
        <td><p>Afmetingen</p></td><td><input type="text" name="eva" id="eva" size="50" required></td>
    </tr>
    <tr>
        <td><p>Voorraad</p></td><td><input type="text" name="vrd" id="vrd" size="10" required></td>
    </tr>
    <tr>
        <td><p>Prijs</p></td><td><input type="text" name="pri" id="pri" size="10" required></td>
    </tr>
    
    <tr>
        <td></td>
        <td><input type="submit" name="send" id="send" value="Toevoegen" style="height:40px;width:120px"><br>
            <input type="button" value="Terug naar startscherm" style="height:40px;width:180px" onclick="location.href='../../startscreen.php','650'">
    </tr>
</table>
</form>
</body>
<script src="../../windowsOpen.js"></script>
</html>