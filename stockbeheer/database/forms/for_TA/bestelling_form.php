<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Formulier bestellingen</title>

<style type="text/css">
body {
    font-family: Segoe UI Semilight;
    color:blue;
    background-color: #b0b0b0;
    
}
p {border-style:double;
    background-color:cyan;
}
</style>
</head>
<body>
<form name="bestelling" method="get" action="../bestelling/bestelling_query.php">
<table>
    <tr>
        <td><p>Bestellingnummer</p></td><td><input type="text" name="blr" id="blr" size="10" required></td>
    </tr>
    <tr>
        <td><p>Bestelaantal</p></td><td><input type="text" name="bla" id="bla" size="5" required></td>
    </tr>
    <tr>
        <td><p>Artikelnaam</p></td><td>
            <select name="arc" id="arc" required>
                <!--Selection of leveranciers-->
                <?php 
                    include_once('../../config/config.php');
                   
                    $artRow = [];
                    $arcArray = [];
                    $arnArray = [];
                    $lvnArray = [];
                    $sql = "SELECT artikels.artRow, artikels.arc, artikels.arn, leveranciers.lvn FROM artikels, leveranciers";
                    $result =mysqli_query($connection,$sql);
                            if (mysqli_query($connection,$sql)){   
                                while($rows=mysqli_fetch_assoc($result)){
                                    array_push($artRow, $rows['artRow']);
                                    array_push($arcArray, $rows['arc']);
                                    array_push($arnArray, $rows['arn']);
                                    array_push($lvnArray, $rows['lvn']);
                                }
                                for($i=0;$i < max($artRow);$i++)
                                {
                echo '<option value='.$arcArray[$i].'>'.$arnArray[$i]." |Leverancier: ".$lvnArray[$i].'</option>';
                                }
                            }   
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td><p>Datum Bestelling</p></td><td><input type="date" name="dte" id="dte" required></td>
    </tr>
    <tr>
        <td><p>Opmerkingen</p></td><td><textarea name="opm" id="opm" rows="4" cols="30" required></textarea></td>
    </tr>
    <tr>
        <td><p>Prioriteit</p></td><td>
            <select name="prt" id="prt" required>
                            <option value="Hoog">Hoge prioriteit</option>
                            <option value="Normaal">Normale prioriteit</option>
                            <option value="Laag">Lage prioriteit</option>
            </select></td>
    </tr>
    <tr>
        <td><p>Code van opvolging</p></td><td>
            <select name="cop" id="cop" required>
                            <option value="O">Offertevraag verstuurd</option>
                            <option value="BB">Bestelbon opgemaakt - bestelling binnen 1 week</option>
                            <option value="L">Levering, eventueel backorder</option>
            </select>
        </td>
    </tr>
    <tr>
        <td><p>Prijs</p></td><td><input type="text" name="pri" id="pri" size="10" required></td>
    </tr>
    
    <tr>
        <td></td>
        <td><input type="submit" name="send" id="send" value="Toevoegen" style="height:40px;width:120px"><br>
            <input type="button" value="Terug naar startscherm" style="height:40px;width:180px" onclick="location.href='../../startscreen.php','650'">
    </tr>
</table>
</form>
</body>
<script src="../../windowsOpen.js"></script>
</html>