<?php 
require_once('config/config.php');
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Startscherm</title>
    <link rel="stylesheet" type="text/css" href="responsive.css">

</style>
</head>
<body>
<table>
<tr>
<td><p>Ingelogde gebruiker: <?php echo $_SESSION['user']; ?></p></td>
</tr>
<tr><!-- opent de handleiding-->
<input type="button" value="Handleiding" style="height:40px;width:120px" onclick="window.open('Handleiding.pdf','_blank')"></td>
</tr>
<tr> <!-- link naar artikels-->
    <td><br><p class="title">Artikels</p>
    <tr><td><a href="tables/artikels_beperkt.php" onclick="location.href('tables/artikels_beperkt.php')">Overzicht</a></td></tr>
    </td><!-- link naar bestelling-->
    <td><br><p class="title">Bestelling</p>
    <tr><td><a href="forms/for_leerkracht/bestelling_form.php" onclick="location.href('forms/for_leerkracht/bestelling_form.php')">Formulier</a></td></tr>
    </td>
</tr>
<tr><!--button voor uitloggen -->
    <td><br><br><input type="button" value="Uitloggen" style="height:40px;width:120px" onclick="location.href='../../index.php','650'">
</tr>
</table>
</div>
<div class="image">
<img src="../../materiaalkoffers/images/leekrachtpicture.jpg" alt="leekracht" width="75%" height= "100%">
</div>
</body>
<script src="windowsOpen.js"></script>
</html>
