<script>
function edit(orderNumber){
  // Get the modal
  var modal = document.getElementById("modalPopUp");
  var pageBody = document.getElementById("pageBody")
  var nest = document.getElementById("nestForWarnings")
  var italic = document.createElement("em")
  var textWarning = "Pas op! Bewerken van deze gegevens kan leiden tot fouten bij artikels en bestellingen. Het is aangeraden om de Leverancierscode niet bewerken!"
  // Get the button that opens the modal
  var btn = document.getElementById("edit");
  // Get the <span> element that closes the modal
  var spanClose = document.getElementsByClassName("close")[0];
  var spanSave = document.getElementsByClassName("save")[0];
  // When the user clicks the button, open the modal 
  btn.onclick = function() {
    pageBody.style.webkitFilter = "blur(20px)";
    modal.style.display = "block";
    nest.prepend(italic)
    italic.append(textWarning)
  }
  // When the user clicks on <span> (x), close the modal
  spanClose.onclick = function() {
    modal.style.display = "none";
    pageBody.style.webkitFilter = "blur(0px)";
    location.reload()
  }
  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
      location.reload()
    }
  }
  //Get the input textboxes
  var textContainerLEV = document.getElementById("textContainerLEV")
  function leveranciers(){
    var list = ["lvc","lvn","str","gmt","eml"]
    var variables = []
    document.getElementById("levRow").setAttribute("value", rows[orderNumber-1])
    for(i=0;i <5;i++){
    variables[i] = document.getElementById(list[i])
  variables[i].setAttribute("value", values[i][orderNumber-1])
    }
  }
  if(typeof(textContainerLEV) != 'undefined' && textContainerLEV != null){
    leveranciers()
    spanSave.onclick = function() {
    }
  }
}
</script>