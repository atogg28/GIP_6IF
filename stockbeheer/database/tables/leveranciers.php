<?php 
include_once('../forms/leveranciers/leveranciers_select.php');
include_once('../forms/leveranciers/leveranciers_ptj.php');
include_once('modals/modalLeverancier.php');
include_once('disableModals/dModalLeverancier.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Leveranciers</title>
<style type="text/css">
body {
    font-family: Segoe UI Semilight;
    color:blue;
    background-color: #b0b0b0;

}
</style>
<link rel="stylesheet" type="text/css" href="modal.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    //Vars of position and new items always inside the function - now are constants
    var rows = levRow
    var number = levRow[levRow.length -1];
    var nritems = 6;
    var attnames = ["leverancierscode","naam","straat","gemeente","mail","status"];
    var realnames = ["Leverancierscode","Naam","Straat","Gemeente","Mail","Status"];
    var idnames = ["lvc0","lvn0","str0","gmt0","eml0","del0"];
    var values = [lvc,lvn,str,gmt,eml,delLev];
</script>
</head>
<body onload="addFields()">
<form id="leveranciers" method="get" action="">
<div id="pageBody">
    <div id="horizontal">
    </div>
    <div id="hideOnPrint">
    <p><u><h2>Opties</h1></u></p>
    <input type="button" id="editing" value="Bewerken" style="height:40px;width:180px" onclick="editAdd()">
    <input type="button" id="deleting" value="Status veranderen" style="height:40px;width:180px" onclick="deleteAdd()" disabled>
    <select id="selectBox" onchange="changeFunction();document.getElementById('deleting').disabled = false" onclick="document.getElementById('tobehidden').style.display = 'none'">
      <option value="default" id="tobehidden"></option>
      <option value="off">Uitschakelen</option>
      <option value="on">Inschakelen</option>
    </select>
    <br>
    <input type="button" value="Terug naar startscherm" style="height:40px;width:180px" onclick="location.href='../startscreen.php','650'">
    <br>
    <input type="button" name="menu" value="Scherm afprinten" style="height:40px;width:180px" onclick="document.getElementById('hideOnPrint').style.display = 'none';window.print();location.reload()">
    </div>
    </div>
    <!-- Modal content -->
    <div id="modalPopUp" class="modal">
    <div class="modal-content" id="nestForWarnings">
    <input type="button" class="button close" value="&#10060;">
    <table>    
    <tr>
    <td><p>Rangnummer</p>
    <td><p>Leverancierscode</p>
    <td><p>Naam</p>
    <td><p>Straat</p>
    <td><p>Gemeente</p>
    <td><p>Mail</p>
    </tr>
    <tr id="textContainerLEV">
    <td><input type="text" name="levRow" id="levRow" readonly>
    <td><input type="text" name="lvc" id="lvc" readonly>
    <td><input type="text" name="lvn" id="lvn">
    <td><input type="text" name="str" id="str">
    <td><input type="text" name="gmt" id="gmt">
    <td><input type="text" name="eml" id="eml">
    </tr>
    <input type="submit" class="button save" value="&#10004;&#65039;">
    </table>
  </div>
</div>
</form>
<script src="listmaker.js"></script>
<script src="changeTypes.js"></script>
<script src="appendEdit.js"></script>
<script src="appendDelete.js"></script>
<script src="../windowsOpen.js"></script>
</body>
</html>
		

