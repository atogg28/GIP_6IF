<?php 
include_once('../forms/bestelling/bestelling_select.php');
include_once('../forms/bestelling/bestelling_ptj.php');
include_once('modals/modalBestelling.php');
include_once('disableModals/dModalBestelling.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bestelling</title>
<style type="text/css">
body {
    font-family: Segoe UI Semilight;
    color:blue;
    background-color: #b0b0b0;

}
</style>

<link rel="stylesheet" type="text/css" href="modal.css">
<script>
    //Vars of position and new items always inside the function - now are constants
    var rows = besRow
    var number = besRow[besRow.length -1];
    var nritems = 9;
    var attnames = ["bestellingnummer","bestelaantal","artikelnaam","datumbestelling","opmerkingen","prioriteit","codeopvolging","prijs","status"];
    var realnames = ["Bestellingnummer","Bestelaantal","Artikelnaam","Datum Bestelling","Opmerkingen","Prioriteit","Code van Opvolging","Prijs","Status"];
    var idnames = ["blr0","bla0","arn0","dte0","opm0","prt0","cop0","pri0","del0"];
    var values = [blr,bla,arnB,dte,opm,prt,cop,pri,delBes];
</script>
</head>
<body onload="addFields()">
<form id="bestelling" method="get" action="">
<div id="pageBody">
    <div id="horizontal">
    </div>
    <div id="hideOnPrint">
    <p><u><h2>Opties</h1></u></p>
    <input type="button" id="editing" value="Bewerken modus" style="height:40px;width:180px" onclick="editAdd()">
    <input type="button" id="deleting" value="Status modus" style="height:40px;width:180px" onclick="deleteAdd()" disabled>
    <select id="selectBox" onchange="changeFunction();document.getElementById('deleting').disabled = false" onclick="document.getElementById('tobehidden').style.display = 'none'">
      <option value="default" id="tobehidden"></option>
      <option value="off">Uitschakelen</option>
      <option value="on">Inschakelen</option>
    </select>
    <br>
    <input type="button" value="Terug naar startscherm" style="height:40px;width:180px" onclick="location.href='../startscreen.php','650'">
    <br>
    <input type="button" name="menu" value="Scherm afprinten" style="height:40px;width:180px" onclick="document.getElementById('hideOnPrint').style.display = 'none';window.print();location.reload()">
    </div>
    </div>
    <!-- Modal content -->
    <div id="modalPopUp" class="modal">
    <div class="modal-content" id="nestForWarnings">
    <input type="button" class="button close" value="&#10060;">
    <table>
    <tr>
    <td><p>Rangnummer</p>
    <td><p>Bestellingnummer</p>
    <td><p>Bestelaantal</p>
    <td><p>Artikelnaam</p>
    <td><p>Datum Bestelling</p>
    <td><p>Opmerkingen</p>
    <td><p>Prioriteit</p>
    <td><p>Code van opvolging</p>
    <td><p>Prijs</p>
    </tr>
    <tr id="textContainerBES">
    <td><input type="text" name="besRow" id="besRow" readonly>
    <td><input type="text" name="blr" id="blr">
    <td><input type="text" name="bla" id="bla">
    <td><select name="arc" id="arc" required onclick="document.getElementById('tobehidden').style.display = 'none'">
      <?php
               include_once('../config/config.php');
               $artRow = [];
               $arcArray = [];
               $arnArray = [];
               $sql = "SELECT artRow, arc, arn FROM artikels";
               $result = mysqli_query($connection,$sql);
                       if (mysqli_query($connection,$sql)){   
                           while($rows=mysqli_fetch_assoc($result)){
                               array_push($artRow, $rows['artRow']);
                               array_push($arcArray, $rows['arc']);
                               array_push($arnArray, $rows['arn']);
                               
                           }
            echo '<option value="" id="tobehidden"></option>';
                           for($i=0;$i < max($artRow);$i++)
                           {
            echo '<option value='.$arcArray[$i].'>'.$arnArray[$i].'</option>';
                           }
                        }
      ?>
        </select>
    <td><input type="date" name="dte" id="dte">
    <td><input type="text" name="opm" id="opm">
    <td><select name="prt" id="prt" required onclick="document.getElementById('tobehidden').style.display = 'none'">
                            <option value="" id="tobehidden"></option>
                            <option value="Hoog">Hoge prioriteit</option>
                            <option value="Normaal">Normale prioriteit</option>
                            <option value="Laag">Lage prioriteit</option>
            </select>
    <td><select name="cop" id="cop" required onclick="document.getElementById('tobehidden').style.display = 'none'">
                            <option value="" id="tobehidden"></option>
                            <option value="O">Offertevraag verstuurd</option>
                            <option value="BB">Bestelbon opgemaakt...</option>
                            <option value="L">Levering, eventueel backorder</option>
            </select>
    <td><input type="text" name="pri" id="pri">
    </tr>
    <input type="submit" class="button save" value="&#10004;&#65039;">
    </table>
  </div>
</div>
</form>
<script src="listmaker.js"></script>
<script src="changeTypes.js"></script>
<script src="appendEdit.js"></script>
<script src="appendDelete.js"></script>
<script src="../windowsOpen.js"></script>
</body>
</html>
		

